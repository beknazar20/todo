import React , { Component } from 'react';
import './todo-list-item.css'
export default class TodoListItem extends Component {


    render(){
        const { label, onDeleted, 
            onToggleDone, onToggleImportant,important,done } = this.props;
        
        let classNames = 'todo-title';
        if(done){
            classNames += ' done'
        }

        if(important){
            classNames += ' important '
        }
      
    return(
        <div className=" todo-list-item">
            <span
             className={classNames}
              onClick={onToggleDone}
              >{label} </span>
            <div>
                <button onClick={onToggleImportant} className="btn btn-success item-btn"><i className="bi bi-arrow-up-circle-fill"></i></button>
                <button onClick={onDeleted} className="btn btn-success item-btn"><i className="bi bi-bucket-fill"></i></button>
            </div>
        </div>
        )
}}


