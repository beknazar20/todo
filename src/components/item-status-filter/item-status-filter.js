import React, {Component} from 'react';


export default class ItemStatusFilter extends Component {
    buttons = [
        {name:'all', label: 'All'},
        {name:'active', label: 'active'},
        {name:'done', label: 'done'}
    ]
    render(){
        const { filter , onFilterChange } = this.props;
        const buttons = this.buttons.map(({name,label}) => {
            const isActive = filter === name;
            const clazz = isActive ? 'btn-info' : 'btn-outline-secondary'
            return (
                <button
                 type="button"  
                 className={`btn ${clazz}`}
                 key={name}
                 onClick={()=> onFilterChange(name)}
                 >{label}</button>
            )
        });
        return (
            <div className="ItemStatusFilter">
                <div className="btn-group" role="group" aria-label="Basic example">
                {buttons}
                </div>
            </div>
        )
    }
}