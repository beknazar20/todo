import React, {Component} from 'react'
import ReactDOM from 'react-dom';
import AppHeader  from '../app-header'
import TodoList from '../todo-list';
import SearchPanel from '../search-panel';
import ItemStatusFilter from '../item-status-filter'
import ItemAddForm from '../item-add-form'

import './app.css'

export default class App extends Component {
 maxId = 100;
state = {
  todoData: [
    this.createTodoItem('Drink water'),
    this.createTodoItem('Drink tea'),
    this.createTodoItem('Drink coffee')
  ],
  term : "",
  filter: 'all'
}

 filter = (items, filter) => {
  switch(filter){
    case 'all' :
      return items; 
    case 'active':
       return items.filter(item => !item.done);
    case 'done':
       return items.filter(item => item.done);
    // default:
    //   items;     
  }
}

createTodoItem(label){
  return {
    label,
    important: false,
    done: false,
    id: this.maxId++
  }
}

deleteItem = (id) => {
  this.setState(({todoData}) => {
    const idx = todoData.findIndex(item => item.id === id)

    const newArray = [...todoData.slice(0,idx), ...todoData.slice(idx + 1)]

    return{
      todoData: newArray
    }
  })
}

addItem = (content) => {
  const newItem = this.createTodoItem(content)

  this.setState(({todoData}) => {
    const newArr = [
      ...todoData,
      newItem
    ];
    return {
      todoData: newArr
    }
  })
}


toggleProperty(arr, id, propName){
  const idx = arr.findIndex(item => item.id === id)
  const oldItem = arr[idx]
  const newItem = {...oldItem, 
    [propName]: !oldItem[propName]}
  return [
    ...arr.slice(0, idx),
    newItem,
    ...arr.slice(idx + 1)
  ]
 
}
onToggleImportant = (id) => {
  this.setState(({todoData}) =>{    
    return {
      todoData: this.toggleProperty(todoData,id, 'important')
    }
  })
}
onToggleDone = (id) => {
  this.setState(({todoData}) =>{    
    return {
      todoData: this.toggleProperty(todoData,id, 'done')
    }
  })

}
search(items, term ){
  if(term.length === 0){
    return items
  }
  return  items.filter( (item) => {
    return item.label.toLowerCase().indexOf(term.toLowerCase()) > -1
  })
}
onSearchChange = (term) => {
  this.setState({term})
}
onFilterChange = (filter) => {
  this.setState({filter})
}
 render(){
   const { todoData, term, filter } = this.state;

   const visibleItems = this.filter(this.search(todoData,term), filter)
  const doneCount = todoData.filter(el => el.done).length
  const todoCount = todoData.length - doneCount

  return ( 
   <div>
    <AppHeader todo={todoCount}  done={doneCount}/>
    <div className="searchFilter">
      <SearchPanel onSearchChange = {this.onSearchChange}/>
      <ItemStatusFilter
      onFilterChange={this.onFilterChange}
       filter={filter}/>
    </div>
    <TodoList
      onDeleted = {this.deleteItem}
      todos={visibleItems}
      onToggleDone={this.onToggleDone}
      onToggleImportant={this.onToggleImportant}

      />
    <ItemAddForm onItemAdded={this.addItem }/>
   </div>
 )
 }
}
