import React, { Component } from 'react';
import './item-add-form.css'
export default class ItemAddForm extends Component {
   
    state = {
        value: ""
    }
    onLableChange = (e) => {
        this.setState({
            value: e.target.value
        })
    }
    onSubmit = (e) => {
        e.preventDefault()
        this.props.onItemAdded(this.state.value)
        this.setState({
            value: ''
        })
    }
    render(){
        return (
            <div className="container btn-add">
                <form onSubmit={this.onSubmit}>
                    <input 
                    className="form-control"
                    placeholder="Whats needs to do ?"
                    onChange={this.onLableChange}
                    value={this.state.value}
                     />
                    <button  
                    className="btn btn-success"
                    
                    >Add item</button>
                </form>
            </div>
        )
    }
}