import React from 'react';

const AppHeader = ({todo,done}) => {
    return  (
      <div>
<h2>Todo App<span className="badge badge-secondary">{todo} more to do {done} done</span></h2>
       
      </div>
    )
  }
  export default AppHeader